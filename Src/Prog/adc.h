// constante de calibration, pour eviter d'avoir a calibrer a chaque demarrage
#define STABLE_ADC_CALIB	61	// valeur observee sur ce micro

#define TIMER6_HZ	(16000)		// attention : rester compatible avec duree handler TIM6
					// = overhead + duree de conversion (voir adc.c)
// canaux ADC
// attention les symboles LL_ADC_CHANNEL_nn ce n'est pas seulement le numero
#define ANALOG_CH0	LL_ADC_CHANNEL_1	// PA0 aka A0
// #define ANALOG_CH1	LL_ADC_CHANNEL_2	// PA1 aka A1
// #define ANALOG_CH0	LL_ADC_CHANNEL_11	// PB0 aka D3
// #define ANALOG_CH1	LL_ADC_CHANNEL_12	// PB1 aka D6
// parametres de decimation
#define KFIR		10	// coeff de decimation par canal

// shared global storage
extern volatile unsigned int fircnt;	// 1 lsb pour le canal
extern volatile unsigned int adc1_res0;
extern volatile unsigned int cycles;	// compte a la frequ. d'ech de 1 canal

// init ADC version toute simple, pour 2 canaux ANALOG_CH0 et ANALOG_CH1
// calibration non incluse a cause de la tempo demandee pour l'ADC regulator
void ADC_init_simple(void);

void ADC_calib(void);

// demarrer APRES calibration
void ADC_start(void);

// conversion bloquante
// unsigned int ADC_do1conv_block(void);
