#ifdef __cplusplus
extern "C" {
#endif

// UART1
void UART1_init( unsigned int bauds );
void UART1_TX_INT_enable(void);
void UART1_TX_INT_disable(void);

// UART2 (interface CDC-USB sur nucleo non modifiee)
void UART2_init( unsigned int bauds );
void UART2_TX_INT_enable(void);
void UART2_TX_INT_disable(void);

// UART3
void UART3_init( unsigned int bauds );
void UART3_TX_INT_enable(void);
void UART3_TX_INT_disable(void);

#ifdef __cplusplus
}
#endif
