#include "stm32f3xx_ll_bus.h"
#include "stm32f3xx_ll_tim.h"

#include "options.h"
#include "timer.h"

// timer 6 = "basic timer" bon pour faire une simple base de temps
//
// il faut avoir d�fini TIM6_DAC1_IRQHandler

// initialise le timer 6 avec les 2 constantes demandees
// toujours soustraire 1 au rapport des frequences
void timer_init_6( unsigned int psc, unsigned int arr )
{
// Enable the timer peripheral clock 
LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM6);
  
LL_TIM_SetPrescaler( TIM6, psc );
  
LL_TIM_SetAutoReload( TIM6, arr );
 
// Enable the update interrupt 
LL_TIM_EnableIT_UPDATE( TIM6 );
  
// Configure the NVIC to handle TIM1 update interrupt 
NVIC_SetPriority( TIM6_DAC_IRQn, PRIO_TIM6 );
NVIC_EnableIRQ( TIM6_DAC_IRQn );
  
// Enable counter 
LL_TIM_EnableCounter( TIM6 );
  
// Force update generation 
// LL_TIM_GenerateEvent_UPDATE( TIM6 );
}

void timer_stop_6(void)
{
LL_TIM_DisableCounter( TIM6 );
LL_TIM_ClearFlag_UPDATE( TIM6 );
}
