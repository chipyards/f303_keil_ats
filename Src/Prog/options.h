/** modules optionnels **/
#define USE_UART1
#define USE_UART2		// CDC vers PC via ST-Link

// #define RX_FIFO
#define USE_LOGFIFO		// fifo forwardable vers transcript et/ou CDC

//#define USE_SPI2SL		// SPI2 en mode slave

// la pin PB4 est en bas a gauche du bouton reset
// #define PROFILER_PB4

/** priorites des interruptions **/
#define PRIO_TIM6	3	// TIM6 pour ADC
#define PRIO_SYSTICK	7
#define PRIO_UART2	9

// Note : systick est prioritaire sur UART2 car l'interpreteur de commande
// appele par uart2 peut invoquer des tempos.
// L'interpreteur peut creer des messages mais c'est non-bloquant grace au fifo
