#define LED_ON()	LL_GPIO_SetOutputPin(    GPIOB, LL_GPIO_PIN_3 )
#define LED_OFF()	LL_GPIO_ResetOutputPin(  GPIOB, LL_GPIO_PIN_3 )

void gpio_init(void);
void gpio_uart1_init(void);
void gpio_uart2_init(void);

#ifdef PROFILER_PB4
__STATIC_INLINE void SET_PROFILER_PB4(void) { LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_4); }
__STATIC_INLINE void RESET_PROFILER_PB4(void) { LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_4); }
__STATIC_INLINE void TOG_PROFILER_PB4(void) { LL_GPIO_TogglePin(GPIOB, LL_GPIO_PIN_4); }
#endif
