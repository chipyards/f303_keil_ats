
/* Includes ------------------------------------------------------------------*/
// #include "stm32f3xx_ll_bus.h"
#include "stm32f3xx_ll_rcc.h"
#include "stm32f3xx_ll_gpio.h"
#include "stm32f3xx_ll_usart.h"
#include "stm32f3xx_ll_adc.h"

#include "options.h"
#include "gpio.h"
#include "uarts.h"
#include "logfifo.h"
#include "adc.h"
#include "timer.h"

void SystemClock_Config_HSI_8(void);
void SystemClock_Config_HSE_10(void);
void SystemClock_Config_HSI_PLL48(void);
void cmd_handler( char c );
void report_interrupts(void);
void ADC_start_sequence( int calib_flag );
void ADC_force_calib_val( unsigned int new_cal );

#define GREEN_CPU

// contexte global -----------------------------------------------------------

volatile unsigned int ticks = 0;

#ifdef RX_FIFO
// reception UART : fifo circulaire rudimentaire
#define QRX (1<<5)		// a power of 2 !!!
char urxbuf[QRX];
volatile unsigned int urxwi=0;	// write index
volatile unsigned int urxri=0;	// read index
// exemple de lecture du fifo  :
// 	while	( urxwi - urxri )
//		{
//		int c = urxbuf[(urxri++)&(QRX-1)];
//		... }
#endif

// systick interrupt handler
void SysTick_Handler()
{
++ticks;
switch	( ticks % 100 )
	{
	case 0 :
	case 10 :
		LED_ON();
		break;
	case 5 :
	case 25 :
		LED_OFF();
		break;
	}
}

#ifdef USE_UART2
// UART2 interrupt handler
void USART2_IRQHandler( void )
{
if	(
	( LL_USART_IsActiveFlag_TXE( USART2 ) ) &&
	( LL_USART_IsEnabledIT_TXE( USART2 ) )
	)
	{
	#ifdef USE_LOGFIFO
	if	( logfifo.rda == logfifo.wra )
		{			// rien a transmettre, attendre
		UART2_TX_INT_disable();
		}
	else	{
		int c;
		c = logfifo.circ[logfifo.rda++];
		if	( c == 0 )
			LL_USART_TransmitData8( USART2, '\n' );
		else	LL_USART_TransmitData8( USART2, c );
		logfifo.rda &= LFIFOMS;
		}
	#else
	LL_USART_TransmitData8( USART2, '?' );
	#endif
	}
if	(
	( LL_USART_IsActiveFlag_RXNE( USART2 ) ) &&
	( LL_USART_IsEnabledIT_RXNE( USART2 ) )
	)
	{
	#ifdef RX_FIFO
	urxbuf[(urxwi++)&(QRX-1)] = LL_USART_ReceiveData8( USART2 );
	#else
	cmd_handler( LL_USART_ReceiveData8( USART2 ) );
	#endif
	}
}
#endif

void cmd_handler( char c )
{
// reponse test pour CDC/USB
if	( c == '?' )
	{
	LOGprint("SystemCoreClock = %u", SystemCoreClock ); 
	report_interrupts();
	// LOGprint("spi2 : rx_wi=%d rx_ri=%d", SPI_2.rx_wi, SPI_2.rx_ri );
	return;
	}
else if	( c == '!' )
	{
	LOGflush(); return;
	}
else if	( c == 'C' )
	{
	ADC_start_sequence(1);
	}
else if	( c == 'X' )
	{
	LOGprint("calfact = %d", (int)ADC1->CALFACT );
	}
else if	( c == '+' )
	{
	ADC_force_calib_val( ADC1->CALFACT + 1 );
	LOGprint("calfact = %d", (int)ADC1->CALFACT );
	}
else if	( c == '-' )
	{
	ADC_force_calib_val( ADC1->CALFACT - 1 );
	LOGprint("calfact = %d", (int)ADC1->CALFACT );
	}
else if	( c == ' ' )
	{
	LOGprint("adc = %5d, cycles = %u", (int)adc1_res0, cycles );
	}
else	{
	if	( c > ' ' )
		LOGputc( c );
	else	LOGprint( " 0x%02x ", c );
	}
// reponse test pour SPI/RASPI : le char est mis dans le fifo tel quel
// spi2_put8( c );
}


#ifdef USE_LOGFIFO
// N.B. pour avoir la correspondance numero <--> perif , voir IRQn_Type
void report_interrupts(void)
{
int i;
unsigned int p;
p = __NVIC_GetPriorityGrouping();
LOGprint("priority grouping %d", p );
// special systick
i = -1;
if	(  SysTick->CTRL & SysTick_CTRL_TICKINT_Msk )
	{
	p = __NVIC_GetPriority((IRQn_Type)i);
	LOGprint("int #%2d, pri %d", i, p );
	}
// tous les autres
for	( i = 0; i <=  97; ++i )
	{
	if	( __NVIC_GetEnableIRQ((IRQn_Type)i) )
		{
		p = __NVIC_GetPriority((IRQn_Type)i);
		LOGprint("int #%2d, pri %d", i, p );
		}
	}
}
#endif

// demarrage ADC reutilisable (utilise ticks pour la tempo)
void ADC_start_sequence( int calib_flag )
{
unsigned int oldt = ticks;
timer_stop_6();
while	( ticks < (oldt+2) )
	{ }
LL_ADC_ClearFlag_EOC(ADC1);
ADC_init_simple();
while	( ticks < (oldt+3) )
	{ }
if	( calib_flag )
	ADC_calib();
else	{
	LL_ADC_ClearFlag_EOC(ADC1);
	ADC1->CALFACT = STABLE_ADC_CALIB;
	}
while	( ticks < (oldt+4) )
	{ }
// demarrer APRES calibration
ADC_start();
// demarrer le timer APRES que l'ADC soit prepare !
// objectif : 500 samples/s soit 8kHz si on surechantillonne de 16, soit 16 kHz pour 2 canaux
timer_init_6( 1-1, (SystemCoreClock / TIMER6_HZ ) - 1 );
}

void ADC_force_calib_val( unsigned int new_cal )
{
unsigned int oldt = ticks;
timer_stop_6();
while	( ticks < (oldt+2) )
	{ }
LL_ADC_ClearFlag_EOC(ADC1);
ADC1->CALFACT = new_cal;
timer_init_6( 1-1, (SystemCoreClock / TIMER6_HZ ) - 1 );
}

int main(void)
{
// Configure the system clock to 48 MHz
SystemClock_Config_HSE_10();
SystemCoreClockUpdate();

gpio_init();

UART2_init( 9600 );

// config systick @ 100Hz

// periode
SysTick->LOAD  = (SystemCoreClock / 100) - 1;
// priorite
NVIC_SetPriority( SysTick_IRQn, PRIO_SYSTICK );
// init counter
SysTick->VAL = 0;
// prescale (0 ===> %8)
SysTick->CTRL = SysTick_CTRL_CLKSOURCE_Msk;
// enable timer, enable interrupt
SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk | SysTick_CTRL_ENABLE_Msk;

// demarrer l'ADC APRES systick (bloque > 40ms)
ADC_start_sequence(0);

 
  /* Infinite loop */
while	(1)
	{
	#ifdef GREEN_CPU
		{
		SCB->SCR = 0;				// avoid deep sleep
		PWR->CR &= ~(PWR_CR_PDDS|PWR_CR_LPDS);	// avoid power down
		__WFI();				// Wait for Interrupt
		}
	#endif
	}
}

void SystemClock_Config_HSI_8(void)
{
/* Set FLASH latency (0 up to 24 MHz)*
LL_FLASH_SetLatency(LL_FLASH_LATENCY_0);
*/

/* Enable HSI and wait for activation*/
LL_RCC_HSI_Enable(); 
while	( LL_RCC_HSI_IsReady() != 1 ) 
	{ }

LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_HSI);
while	( LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_HSI )
	{ }
 
// Set APB1 & APB2 prescaler
LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1); 
}

void SystemClock_Config_HSE_10(void)
{
/* Set FLASH latency (0 up to 24 MHz)*
LL_FLASH_SetLatency(LL_FLASH_LATENCY_2);
*/

/* Enable HSE */
LL_RCC_HSE_Enable();
while	(LL_RCC_HSE_IsReady() != 1)
	{ }
LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_HSE);
while	( LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_HSE )
	{ }

// Set APB1 & APB2 prescaler
LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1); 
}

#ifdef USE_PLL
#include "stm32f3xx_ll_system.h"

void SystemClock_Config_HSI_PLL48(void)
{
/* Set FLASH latency (0 up to 24 MHz)*/
LL_FLASH_SetLatency(LL_FLASH_LATENCY_2);

/* Enable HSI and wait for activation*/
LL_RCC_HSI_Enable(); 
while	(LL_RCC_HSI_IsReady() != 1) 
	{ }

/* Main PLL configuration and activation */
LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSI_DIV_2, LL_RCC_PLL_MUL_12);
  
LL_RCC_PLL_Enable();
while	(LL_RCC_PLL_IsReady() != 1)
	{ }
  
// Sysclk activation on the main PLL
LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);
while	(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL)
	{ }
 
// Set APB1 & APB2 prescaler
LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1); 
}
#endif

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif
