#include "stm32f3xx_ll_bus.h"
#include "stm32f3xx_ll_usart.h"

#include "options.h"
#include "uarts.h"

// code d'initialisation communs aux UARTs
static void UART_8_N_1( USART_TypeDef * U, unsigned int bauds )
{
/* ne pas utiliser
   LL_USART_Init( U, &usart_initstruct);
   hautement toxique!! cette fonction utilise une valeur de HSE_VALUE codee
   en dur dans stm32f3xx_ll_rcc.h qu'on ne peut pas overrider
 */
LL_USART_SetBaudRate( U, SystemCoreClock, LL_USART_OVERSAMPLING_16, 9600 );
LL_USART_SetDataWidth( U, LL_USART_DATAWIDTH_8B );
LL_USART_SetStopBitsLength( U, LL_USART_STOPBITS_1 );
LL_USART_SetParity( U, LL_USART_PARITY_NONE );
LL_USART_SetTransferDirection( U, LL_USART_DIRECTION_TX_RX );
LL_USART_SetHWFlowCtrl( U, LL_USART_HWCONTROL_NONE );
LL_USART_SetOverSampling( U, LL_USART_OVERSAMPLING_16 );

LL_USART_Enable( U );
// wait
while	(
	(!(LL_USART_IsActiveFlag_TEACK( U ))) ||
	(!(LL_USART_IsActiveFlag_REACK( U )))
	)  {}
}

// config d'un canal d'interruption
static void NVIC_init( IRQn_Type IRQn, int prio )
{
NVIC_SetPriority( IRQn, prio );  
NVIC_EnableIRQ( IRQn );
}

#ifdef USE_UART1
// UART1 en mode interruption ==============================================================
// la fonction USART1_IRQHandler() doit etre d�finie par ailleurs
#include "stm32f3xx_ll_rcc.h"

void UART1_init( unsigned int bauds )
{
LL_APB2_GRP1_EnableClock( LL_APB2_GRP1_PERIPH_USART1 );
LL_RCC_SetUSARTClockSource( LL_RCC_USART1_CLKSOURCE_PCLK1 );
UART_8_N_1( USART1, bauds );

NVIC_init( USART1_IRQn, 9 );
NVIC_ClearPendingIRQ( USART1_IRQn );
/* Enable USART1 Receive interrupts --> USART1_IRQHandler */
LL_USART_EnableIT_RXNE( USART1 );
// N.B. interruption TX sera validee a la demande
}

// mise en route de l'emission par interruption
void UART1_TX_INT_enable()
{
LL_USART_EnableIT_TXE( USART1 );
}

// arret de l'emission par interruption
void UART1_TX_INT_disable()
{
LL_USART_DisableIT_TXE( USART1 );
}
#endif

// UART2 en mode interruption ==============================================================
// la fonction USART2_IRQHandler() doit etre d�finie par ailleurs

// N.B. la pin correpondant a la sortie TX doit etre configuree en alternate push pull
//      la pin correpondant a l'entree RX doit etre configuree en alternate
void UART2_init( unsigned int bauds )
{
LL_APB1_GRP1_EnableClock( LL_APB1_GRP1_PERIPH_USART2 );
// USART2 est toujours sur PCLK1, pas de mux
// LL_RCC_SetUSARTClockSource( LL_RCC_USART2_CLKSOURCE_PCLK1 );
UART_8_N_1( USART2, bauds );

NVIC_init( USART2_IRQn, PRIO_UART2 );
NVIC_ClearPendingIRQ( USART2_IRQn );
/* Enable USART2 Receive interrupts --> USART2_IRQHandler */
LL_USART_EnableIT_RXNE( USART2 );
// N.B. interruption TX sera validee a la demande
}

// mise en route de l'emission par interruption
void UART2_TX_INT_enable()
{
LL_USART_EnableIT_TXE( USART2 );
}

// arret de l'emission par interruption
void UART2_TX_INT_disable()
{
LL_USART_DisableIT_TXE( USART2 );
}

