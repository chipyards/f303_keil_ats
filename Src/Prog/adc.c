#include "stm32f3xx_ll_bus.h"
#include "stm32f3xx_ll_rcc.h"
#include "stm32f3xx_ll_adc.h"
#include "stm32f3xx_ll_tim.h"
#include "options.h"
#ifdef PROFILER_PB4
#include "stm32f3xx_ll_gpio.h"
#include "gpio.h"
#endif
#include "adc.h"

// shared global storage
volatile unsigned int fircnt = 0;		// 1 lsb pour le canal
volatile unsigned int adc1_res0 = 0;
volatile unsigned int cycles = 0;

// interrupt handler for TIM6 (a "basic" timer)
void TIM6_DAC1_IRQHandler(void)
{
static unsigned int adc1_sum0 = 0;

#ifdef PROFILER_PB4
SET_PROFILER_PB4();
#endif
// acquitter l'interrupt
LL_TIM_ClearFlag_UPDATE( TIM6 );
// recuperer resultat derniere conversion
adc1_sum0 += LL_ADC_REG_ReadConversionData12(ADC1);
// compter
++fircnt;
// conclure la decimation  s'il y a lieu
if	( fircnt >= KFIR )
	{
	// #ifdef PROFILER_PB4 // pour le periodemetre
	// TOG_PROFILER_PB4();
	// #endif
	fircnt = 0; cycles += 1;
	adc1_res0 = adc1_sum0;
	adc1_sum0 = 0;
	}
// demarrer nouvelle conversion
LL_ADC_REG_StartConversion(ADC1);
/* verif timing : bloquage pendant la conversion pour mesure duree conversion a l'oscillo
// duree de conversion ( 12.5 + sampling_time ) * Tckadc
// exemple Tckadc = 2/10MHz (TCXO @ 10MHz, DIV2)
// 	(181.5 + 12.5) * (2/10M) = 38.8us
// exemple Tckadc = 1/8MHz (HSI @ 8MHz)
// 	(181.5 + 12.5) * (1/8M) = 24.25us
// mesure @  8MHz DIV1 : 10us alterne avec 35us ==> confirme Ok
// mesure @ 10MHz DIV2 : 8us  alterne avec 48us ==> confirme Ok
*/
#ifdef PROFILER_PB4
if	( fircnt & 1 )	// 1 fois sur 2
	{
	while	( LL_ADC_IsActiveFlag_EOC(ADC1) == 0 )
		{}
	}
RESET_PROFILER_PB4();
#endif
}


// init ADC version toute simple, pour 2 canaux ANALOG_CH0 et ANALOG_CH1
// calibration non incluse a cause de la tempo demandee pour l'ADC regulator
void ADC_init_simple(void)
{
// on disable car certains settings sont inoperants si l'ADC est enabled
LL_ADC_Disable(ADC1);

// Enable ADC clock (core clock) 
LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_ADC12);
 
/*
Set ADC clock (conversion clock) common to several ADC instances 
cet ADC supporte 72 MHz !!!
on peut diviser HCLK (=PCLK) par 1, 2 ou 4, ou alors choisir une clock "asynchrone"
Synchrone c'est mieux pour la fidelite du timing.
01: HCLK/1 (Synchronous clock mode). This configuration must be enabled only if the AHB
clock prescaler is set to 1 (HPRE[3:0] = 0xxx in RCC_CFGR register) and if the system clock
has a 50% duty cycle.
10: HCLK/2 (Synchronous clock mode)
11: HCLK/4 (Synchronous clock mode)
Explication : le AHB prescaler ne donnerait pas un duty cycle de 50% quand il divise.
Vu que le TCXO ne garantit pas le duty cycle, on met DIV2 par precaution
*/
LL_ADC_SetCommonClock( __LL_ADC_COMMON_INSTANCE(ADC1), LL_ADC_CLOCK_SYNC_PCLK_DIV2 );
   
LL_ADC_SetResolution( ADC1, LL_ADC_RESOLUTION_12B );
LL_ADC_SetDataAlignment( ADC1, LL_ADC_DATA_ALIGN_RIGHT );

// Set ADC group regular trigger source 
LL_ADC_REG_SetTriggerSource( ADC1, LL_ADC_REG_TRIG_SOFTWARE );
    
// Set ADC group regular continuous mode 
LL_ADC_REG_SetContinuousMode( ADC1, LL_ADC_REG_CONV_SINGLE );

// Set ADC group regular overrun behavior 
LL_ADC_REG_SetOverrun( ADC1, LL_ADC_REG_OVR_DATA_OVERWRITTEN );

// Set ADC group regular sequencer length and scan direction 
LL_ADC_REG_SetSequencerLength( ADC1, LL_ADC_REG_SEQ_SCAN_DISABLE );

// Set ADC group regular sequence: channel on the selected sequence rank. 
// ben c'est ici qu'on choisit le canal quoi !
LL_ADC_REG_SetSequencerRanks( ADC1, LL_ADC_REG_RANK_1, ANALOG_CH0 );

// Set ADC channels sampling time cf relation avec resistance source RAIN
LL_ADC_SetChannelSamplingTime(ADC1, ANALOG_CH0, LL_ADC_SAMPLINGTIME_181CYCLES_5);

// Enable ADC internal voltage regulator 
LL_ADC_EnableInternalRegulator(ADC1);
/////// ici il faut une tempo !!!!! LL_ADC_DELAY_INTERNAL_REGUL_STAB_US us soit 10us
}

void ADC_calib(void)
{
// Run ADC self calibration 
LL_ADC_StartCalibration(ADC1, LL_ADC_SINGLE_ENDED);
while	( LL_ADC_IsCalibrationOnGoing(ADC1) != 0 )
	{ }
/////// ici il faut une tempo !!!!! LL_ADC_DELAY_CALIB_ENABLE_ADC_CYCLES cycles de ADC clock
}

void ADC_start(void)
{
// Enable ADC 
LL_ADC_Enable(ADC1);
while	( LL_ADC_IsActiveFlag_ADRDY(ADC1) == 0 )
	{ }
}

/* conversion bloquante
unsigned int ADC_do1conv_block(void)
{
// petite precaution car si l'ADC n'est pas demarre la boucle EOC va planter
if	( LL_ADC_IsActiveFlag_ADRDY(ADC1) == 0 )
	return 666666;
// on y va
LL_ADC_REG_StartConversion(ADC1);
while	( LL_ADC_IsActiveFlag_EOC(ADC1) == 0 )
	{ }
// Retrieve ADC conversion data (clears EOC I presume)
return LL_ADC_REG_ReadConversionData12(ADC1);
}
*/
