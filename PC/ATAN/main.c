#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "iatan2.h"

#ifdef HARD_TABLE
#include "tatan7.c"
#else
short unsigned int tatan[(1<<QT)+2];

// remplir la table tatan[]
// entree en QT non signe (peut atteindre QT+1 bits a la limite)
// 0 <= tan <= ((1<<QT)+1)
// sortie t.q. 2PI <==> 1<<QA
// 0 <= tatan[tan] <= (1<<(QA-3))
void tatan_init( int verbose )
{
int tan; double dtan, da;
if	( verbose )
	printf("// QT = %d\nconst short unsigned int tatan[] = {\n", QT );
for	( tan = 0; tan < ((1<<QT)+2); ++tan )
	{
	dtan = (double)tan;
	dtan /= (double)(1<<QT);
	da = atan2( dtan, 1.0 );	// 0 <= da <= pi/4
	tatan[tan] = (int)round((1<<(QA-1))*da/M_PI);
	if	( verbose )
		printf("0x%04x,\n", tatan[tan] );
	}
if	( verbose )
	printf("};\n");
}
#endif

// test : rend l'erreur en ppm de radian
double test1( int Im, int Re, int verbose )
{
int a;
double da, dda, err;
a = iatan2( Im, Re );
dda = (double)a * M_PI / (double)(1<<(QA-1));
da = atan2( (double)Im, (double)Re );
if	( da < 0.0 )
	da += ( 2.0 * M_PI );
err = dda - da;
err *= 1000000;
if	( verbose )
	{
	// printf("%7d / %7d --> %5d   %g vs %g\n", Im, Re, a, dda, da );
	printf("%7d / %7d --> %5d   %g\n", Im, Re, a, round(err) );
	}
return err; 
}



int main()
{
#ifndef HARD_TABLE
tatan_init( 1 );
#endif
int Re, Im;
printf("iatan2 avec table de %d entrees, angles en Q%d\n", sizeof(tatan)/sizeof(short), QA );
printf("milieux d'octants :\n");
test1(  1,  2, 1 );
test1(  2,  1, 1 );
test1(  2, -1, 1 );
test1(  1, -2, 1 );
test1( -1, -2, 1 );
test1( -2, -1, 1 );
test1( -2,  1, 1 );
test1( -1,  2, 1 );

printf("cas triviaux :\n");
test1(  0,  1,  1 );
test1(  1,  1 , 1 );
test1(  1,  0 , 1 );
test1(  1, -1 , 1 );
test1(  0, -1 , 1 );
test1( -1, -1 , 1 );
test1( -1,  0 , 1 );
test1( -1,  1 , 1 );
test1( -1, 100000, 1 );	// risque de rendre 2pi
printf("crash tests :\n");
test1(  0,  0 , 1 );
test1(  32767,  32767 , 1 );
test1( -32767, -32767 , 1 );
test1( -32768, 0 , 1 );
test1( 0, -32768 , 1 );
test1( -32768, -32768 , 1 );


// interpol test
int err, maxerr = 0;
for	( Re = -1000; Re < 1000; Re += 1 )
	{
	Im = 333;
	err = (int)fabs(round(test1( Im, Re , 0 )));
	if	( err > maxerr ) maxerr = err;
	}
printf("interpol test : max err = %d ppm\n", maxerr );

// random test
// c'est la loose chez MinGW : RAND_MAX est 32767  :-( et lrand48 n'existe pas
int i; short sr, si;
maxerr = 0;
for	( i = 0; i < 100000; ++i )
	{
	sr = (rand()<<1) & 0xFFFF;
	si = (rand()<<1) & 0xFFFF;
	err = (int)fabs(round(test1(  (int)si, (int)sr, 0 )));
	if	( err > maxerr ) maxerr = err;
	}
printf("random test [%d] : max err = %d ppm\n", RAND_MAX, maxerr );
return 0;
}
