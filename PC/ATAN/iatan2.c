#include "iatan2.h"

// fonction atan sur de 0 a pi/4
// entree en Q16 non signe (peut atteindre 17 bits a la limite)
// 0 <= tan <= (1<<16)
// sortie t.q. 2PI <==> 1<<QA
// 0 <= atan1 <= (1<<(QA-3))
// l'interpolation va faire gagner QI bits
static int atan1( int tan )
{
int tanLSBs, premier, second, diff;
tanLSBs = tan & ((1<<QI)-1);	// masquage des QI LSBs
tan >>= QI;			// garder les QT MSBs
second = tatan[tan+1];
premier = tatan[tan];
diff = second - premier;	// fonc. croissante ==> diff > 0
diff *= tanLSBs;
diff += (1<<(QI-1));		// arrondi
diff >>= QI;
return( premier + diff );
}

// Re et Im sur 16 bits signes
// sortie t.q. 2PI <==> 1<<QA
int iatan2( int Im, int Re )
{
int oct;	// numero d'octant
int comp = 0;	// complementation a pi/2
// symetrisation en vue du changement de signe
if	( Im == -32768 )	// cas non support� :-( sorry
	Im = -32767;
if	( Re == -32768 )
	Re = -32767;
// normalisation de Re et Im dans le premier octant ----------
if	( Re >= 0 )
	{
	if	( Im >= 0 )
		{ oct = 0;		}
	else	{ oct = 6; Im = -Im;	}
	}
else	{
	if	( Im >= 0 )
		{ oct = 2; 		}
	else	{ oct = 4; Im = -Im;	}
	Re = -Re;
	}
if	( Im > Re )
	{ comp = 1; int tmp = Re; Re = Im; Im = tmp; }
if	( Re == 0 )
	return 0;	// on renonce si Re et Im sont nuls
// calcul du le numero d'octant ------------------------------
if	( oct & 2 )	// pour oct = 2 et 6
	comp ^= 1;
oct |= comp;		// numero d'octant pret
// calcul de l'angle dans le premier octant ------------------
int tan, iatan2;			// ici Im <= Re 
tan = ( Im << 16 ) / Re;	// tan <= (1<<16) i.e. Q16 non signe
iatan2 = atan1( tan );
// restitution de l'angle, entre 0 et 2pi -------------------
if	( oct & 1 )
	iatan2 = PISUR2 - iatan2;	// complementation
iatan2 += ( PISUR2 * ( oct >> 1 ) );	// rotation
iatan2 &= ((1<<QA)-1);			// eviter de rendre 2pi
return iatan2;
}