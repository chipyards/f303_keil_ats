// mesure des angles
#define QA	16	// t.q. 2PI <==> 1<<QA
			// le max est 18 a cause de la table qui est en unsigned short
#define PISUR2	(1<<(QA-2))
// mesure des rapports
#define QT	7	// entree dans table atan t.q. 1 <==> 1<<QT
			// avec QA = 18, on ne gagne presque rien au dela de 8
			// avec QA = 16, on ne gagne rien au dela de 7
#define QI	(16-QT)	// nombre de lSBs soumis a interpolation

#define HARD_TABLE	// utiliser une table preparee en C

#ifdef HARD_TABLE
extern const short unsigned int tatan[(1<<QT)+2];
#else
extern short unsigned int tatan[(1<<QT)+2];
#endif

// Re et Im sur 16 bits signes
// sortie t.q. 2PI <==> 1<<QA
int iatan2( int Im, int Re );
